// Timer 1: PWM output
// Timer 6: 100kHz sampling timer

#include "DSP28x_Project.h"     // Device Headerfile and Examples Include File
//#include "V2F_LUT.h"

// Configure which ePWM timer interrupts are enabled at the PIE level:
// 1 = enabled,  0 = disabled
#define PWM1_INT_ENABLE  1
#define ADC_INT_ENABLE  1

#define FREQ_ADC	0x9 //0x0 for A0; 0x1 for A1; 0x2 for A2; etc.

typedef struct
{
   volatile struct EPWM_REGS *EPwmRegHandle;
   Uint16 EPwmTimerIntCount;
}EPWM_INFO;
#define EPWM_TIMER_TBPRD_DFT		2250  	// Default 75kHz
#define EPWM_CMP_DFT				1125
#define EPWM_MAX_TIMER_TBPRD		3000	// Min freq: 50kHz
#define EPWM_MIN_TIMER_TBPRD		1500	// Max freq 100kHz
#define EPWM6_TIMER_TBPRD			1500  	// ADC 100kHz
#define EPWM6_CMP					750

Uint16 EPWM_TIMER_TBPRD;
Uint16 EPWM_CMP;

EPWM_INFO epwm1_info;

// Prototype statements for functions found within this file.
interrupt void epwm1_timer_isr(void);
interrupt void adc_isr(void);

void InitEPwm1Timer(void);
void InitEPwm6Timer(void);
void ADC_config(void);

// Global variables used in this example
Uint32  EPwm1TimerIntCount;
Uint32  EPwm6TimerIntCount;

Uint16 ADC_reading;


void main(void)
{
   int i;

// Step 1. Initialize System Control:
// PLL, WatchDog, enable Peripheral Clocks
// This example function is found in the DSP2833x_SysCtrl.c file.
   InitSysCtrl();

   //ADC FREQUENCY
   EALLOW;
   #if (CPU_FRQ_150MHZ)     // Default - 150 MHz SYSCLKOUT
     #define ADC_MODCLK 0x3 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 150/(2*3)   = 25.0 MHz
   #endif
   #if (CPU_FRQ_100MHZ)
     #define ADC_MODCLK 0x2 // HSPCLK = SYSCLKOUT/2*ADC_MODCLK2 = 100/(2*2)   = 25.0 MHz
   #endif
   EDIS;
   EALLOW;
   SysCtrlRegs.HISPCP.all = ADC_MODCLK;
   EDIS;

// Step 2. Initalize GPIO:
// This example function is found in the DSP2833x_Gpio.c file and
// illustrates how to set the GPIO to it's default state.
   InitEPwm1Gpio();
//   InitEPwm6Gpio();


// Step 3. Clear all interrupts and initialize PIE vector table:
// Disable CPU interrupts
   DINT;

// Initialize the PIE control registers to their default state.
// The default state is all PIE interrupts disabled and flags
// are cleared.
// This function is found in the DSP2833x_PieCtrl.c file.
   InitPieCtrl();

// Disable CPU interrupts and clear all CPU interrupt flags:
   IER = 0x0000;
   IFR = 0x0000;

// Initialize the PIE vector table with pointers to the shell Interrupt
// Service Routines (ISR).
// This will populate the entire table, even if the interrupt
// is not used in this example.  This is useful for debug purposes.
// The shell ISR routines are found in DSP2833x_DefaultIsr.c.
// This function is found in DSP2833x_PieVect.c.
   InitPieVectTable();

// Interrupts that are used in this example are re-mapped to
// ISR functions found within this file.
   EALLOW;  // This is needed to write to EALLOW protected registers
   PieVectTable.EPWM1_INT = &epwm1_timer_isr;
   PieVectTable.ADCINT = &adc_isr;
   EDIS;    // This is needed to disable write to EALLOW protected registers

// Step 4. Initialize all the Device Peripherals:
// This function is found in DSP2833x_InitPeripherals.c
// InitPeripherals();  // Not required for this example
   InitEPwm1Timer();
   InitEPwm6Timer();
   InitAdc();

// Step 5. User specific code, enable interrupts:

// Initalize counters:
   EPwm1TimerIntCount = 0;

// Enable CPU INT3 which is connected to EPWM1-6 INT:
   IER |= M_INT3;
// Enable EPWM INTn in the PIE: Group 3 interrupt 1
   PieCtrlRegs.PIEIER3.bit.INTx1 = PWM1_INT_ENABLE;

// Enable CPU INT1 which is connected to ADC INT:
   IER |= M_INT1;
// Enable ADC INTn in the PIE: Group 1 interrupt 6
   PieCtrlRegs.PIEIER1.bit.INTx6 = ADC_INT_ENABLE;

   ADC_config();

// Enable global Interrupts and higher priority real-time debug events:
   EINT;   // Enable Global interrupt INTM
   ERTM;   // Enable Global realtime interrupt DBGM

// Step 6. IDLE loop. Just sit and loop forever (optional):
   for(;;)
   {
       asm("          NOP");
       for(i=1;i<=10;i++)
       {}
   }

}


void InitEPwm1Timer()
{

   EALLOW;
   SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
   EDIS;


   EPwm1Regs.TBPRD = EPWM_TIMER_TBPRD_DFT;        // Set timer period 801 TBCLKs
   EPwm1Regs.TBPHS.half.TBPHS = 0x0000;           // Phase is 0
   EPwm1Regs.TBCTR = 0x0000;                      // Clear counter
   // Set Compare values
   EPwm1Regs.CMPA.half.CMPA = EPWM_CMP_DFT;     // Set compare A value
   EPwm1Regs.CMPB = EPWM_CMP_DFT;               // Set Compare B value
   // Setup counter mode
   EPwm1Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; 	  // Count up
   EPwm1Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
   EPwm1Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
   EPwm1Regs.TBCTL.bit.CLKDIV = TB_DIV1;
   // Setup shadowing
   EPwm1Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
   EPwm1Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
   EPwm1Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
   EPwm1Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;
   // Set actions
   EPwm1Regs.AQCTLA.bit.CAU = AQ_CLEAR;             // Set PWM1A on event A, up count
   EPwm1Regs.AQCTLA.bit.ZRO = AQ_SET;           	// Clear PWM1A on event A, down count
   EPwm1Regs.AQCTLB.bit.CBU = AQ_CLEAR;             // Set PWM1B on event B, up count
   EPwm1Regs.AQCTLB.bit.ZRO = AQ_SET;           	// Clear PWM1B on event B, down count

   EPwm1Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;      // Enable INT on Zero event
   EPwm1Regs.ETSEL.bit.INTEN = PWM1_INT_ENABLE;   // Enable INT
   EPwm1Regs.ETPS.bit.INTPRD = ET_1ST;            // Generate INT on 1st event

   epwm1_info.EPwmTimerIntCount = 0;               // Zero the interrupt counter
   epwm1_info.EPwmRegHandle = &EPwm1Regs;          // Set the pointer to the ePWM module

   EALLOW;
   SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
   EDIS;


}

void InitEPwm6Timer()
{

   EALLOW;
   SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 0;      // Stop all the TB clocks
   EDIS;


   EPwm6Regs.TBPRD = EPWM6_TIMER_TBPRD;           // Set timer period 801 TBCLKs
   EPwm6Regs.TBPHS.half.TBPHS = 0x0000;           // Phase is 0
   EPwm6Regs.TBCTR = 0x0000;                      // Clear counter
   // Set Compare values
   EPwm6Regs.CMPA.half.CMPA = EPWM6_CMP;     // Set compare A value
   EPwm6Regs.CMPB = EPWM6_CMP;               // Set Compare B value
   // Setup counter mode
   EPwm6Regs.TBCTL.bit.CTRMODE = TB_COUNT_UP; 	  // Count up
   EPwm6Regs.TBCTL.bit.PHSEN = TB_DISABLE;        // Disable phase loading
   EPwm6Regs.TBCTL.bit.HSPCLKDIV = TB_DIV1;       // Clock ratio to SYSCLKOUT
   EPwm6Regs.TBCTL.bit.CLKDIV = TB_DIV1;
   // Setup shadowing
   EPwm6Regs.CMPCTL.bit.SHDWAMODE = CC_SHADOW;
   EPwm6Regs.CMPCTL.bit.SHDWBMODE = CC_SHADOW;
   EPwm6Regs.CMPCTL.bit.LOADAMODE = CC_CTR_ZERO;  // Load on Zero
   EPwm6Regs.CMPCTL.bit.LOADBMODE = CC_CTR_ZERO;

   EPwm6Regs.ETSEL.bit.INTSEL = ET_CTR_ZERO;      // Enable INT on Zero event
   EPwm6Regs.ETSEL.bit.INTEN = PWM1_INT_ENABLE;   // Enable INT
   EPwm6Regs.ETPS.bit.INTPRD = ET_1ST;            // Generate INT on 1st event

   EALLOW;
   SysCtrlRegs.PCLKCR0.bit.TBCLKSYNC = 1;         // Start all the timers synced
   EDIS;


}

void ADC_config(void)
{
	// Configure ADC
	   AdcRegs.ADCMAXCONV.all = 0x0000;       // Setup 8 conv's on SEQ1
	   AdcRegs.ADCCHSELSEQ1.bit.CONV00 = FREQ_ADC; // Setup D as 1st SEQ1 conv.
	   AdcRegs.ADCTRL2.bit.EPWM_SOCA_SEQ1 = 1;// Enable SOCA from ePWM to start SEQ1
	   AdcRegs.ADCTRL2.bit.INT_ENA_SEQ1 = 1;  // Enable SEQ1 interrupt (every EOS)

	// Assumes ePWM6 clock is already enabled in InitSysCtrl();
	   EPwm6Regs.ETSEL.bit.SOCAEN = 1;        // Enable SOC on A group
	   EPwm6Regs.ETSEL.bit.SOCASEL = ET_CTRU_CMPA;       // Select SOC on CMPA up //JJ
	   EPwm6Regs.ETPS.bit.SOCAPRD = ET_1ST;        // Generate pulse on 3RD event
}


interrupt void  adc_isr(void)
{
	ADC_reading = AdcRegs.ADCRESULT0 >> 4;

	if (ADC_reading > 2482) {// V>2V
		if (ADC_reading <3724) {// V<3V
			EPWM_TIMER_TBPRD = 3723636 / (ADC_reading - 1241);
		}
		else{
			EPWM_TIMER_TBPRD = EPWM_MIN_TIMER_TBPRD;
		}
	}
	else{
		EPWM_TIMER_TBPRD = EPWM_MAX_TIMER_TBPRD;
	}
	EPWM_CMP = EPWM_TIMER_TBPRD/2;

    // Reinitialize for next ADC sequence
    AdcRegs.ADCTRL2.bit.RST_SEQ1 = 1;         // Reset SEQ1
    AdcRegs.ADCST.bit.INT_SEQ1_CLR = 1;       // Clear INT SEQ1 bit
    PieCtrlRegs.PIEACK.all = PIEACK_GROUP1;   // Acknowledge interrupt to PIE

    return;
}



// Interrupt routines uses in this example:
interrupt void epwm1_timer_isr(void)
{
   EPwm1TimerIntCount++;

   epwm1_info.EPwmRegHandle->TBPRD = EPWM_TIMER_TBPRD;
   epwm1_info.EPwmRegHandle->CMPA.half.CMPA = EPWM_CMP;

   // Clear INT flag for this timer
   EPwm1Regs.ETCLR.bit.INT = 1;

   // Acknowledge this interrupt to receive more interrupts from group 3
   PieCtrlRegs.PIEACK.all = PIEACK_GROUP3;
}



//===========================================================================
// No more.
//===========================================================================
